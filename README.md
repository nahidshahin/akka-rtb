Intro:
=

This RTB agent uses Scala, Akka HTTP for HTTP server and Akka Actors for handling object creations and bid matching. All objects are saved in a list. Script for both object creation and request sending is provided. A small configuration file at src/main/resources/application.conf is also provided to configure HTTP port and host.

Prerequisite:
=
Following list of software is expected to be installed in the system:
* JRE
* Scala 
* sbt 

Build:
=
To build the project run following command from terminal (from project root):

```
 $ sbt update
 $ sbt assembly
```

Run the application:
=

Give following command to run the application from terminal (from project root):

```
$java -jar target/scala-<version>/akka-rtb-assembly-1.0.jar
```


Initialize data:
=
Run following script to initialize the application (from project root):

```
$. /scripts/data_init.sh
```

Output:
-
1th site created
2th site created
3th site created
1th geo created
2th geo created
3th geo created
1th device created
1th banner created
2th banner created
1th campaign created


Bid request:
=

Run following script to to make a bid request (from project root):

```
$. /scripts/request.sh
```

Output:
-

```
{"id":"resp1","bidRequestId":"req1","price":1.1,"adid":"1"}
{"id":"NOT Found","bidRequestId":"","price":0.0}
```
Here first request was successful but second request failed to find any match.


Regarding data structure of targetedSiteIds: 
=

As data type of targetedSiteIds, we can use scala.collection.mutable.TreeSet. TreeSet use a mutable red-black tree as underlying data structure. So it's very fast for searching, O(lg(n)) in the worst case.


Future Improvement suggestion:
=

* Refactor: For the simplicity and time constraint, all classes and utility is in the same file. An immediate improvement would be to refactor it in several files.

* Better handling of match NOT Found with 204 status code

* Improve the BidMatcherActor, currently it only checked banner height, width and bid amount.

* Work on targetedSiteIds data type improvement. If we want to support string ID the  Trie (Keyword Tree) could be another option.

* Add more variable at Targeting class

