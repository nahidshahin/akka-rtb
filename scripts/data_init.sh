#!/bin/bash
# Create site
curl -H "Content-Type: application/json" -X POST -d '{"id":1,"domain":"http://website1.com"}' http://localhost:8080/create-site
curl -H "Content-Type: application/json" -X POST -d '{"id":2,"domain":"http://website2.com"}' http://localhost:8080/create-site
curl -H "Content-Type: application/json" -X POST -d '{"id":3,"domain":"http://website3.com"}' http://localhost:8080/create-site
# Create geos
curl -H "Content-Type: application/json" -X POST -d '{"country":"Bangldesh","city":"Dhaka","lat":30.123,"lon":-10.123}' http://localhost:8080/create-geo
curl -H "Content-Type: application/json" -X POST -d '{"country":"Bangldesh","city":"Comilla","lat":30.123,"lon":-10.123}' http://localhost:8080/create-geo
curl -H "Content-Type: application/json" -X POST -d '{"country":"Bangldesh","city":"Barisal","lat":30.123,"lon":-10.123}' http://localhost:8080/create-geo
# Create device
curl -H "Content-Type: application/json" -X POST -d '{"id":"d123","geo":{"country":"Bangldesh","city":"Dhaka","lat":30.123,"lon":-10.123}}' http://localhost:8080/create-device
# Create banners
curl -H "Content-Type: application/json" -X POST -d '{"src":"website1","id":1,"width":100,"height":100}' http://localhost:8080/create-banner
curl -H "Content-Type: application/json" -X POST -d '{"src":"website2","id":2,"width":120,"height":120}' http://localhost:8080/create-banner
# Create campaign
curl -H "Content-Type: application/json" -X POST -d '{"id":1,"userId":1,"country":"Bangldesh","targeting":{"cities":["Dhaka","Comilla","Barisal"],"targetedSiteIds":[1,2,3]},"banners":[1,2],"bid":1.10}' http://localhost:8080/create-campaign
