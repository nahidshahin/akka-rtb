package com.eskimi

import akka.actor._
import akka.actor.SupervisorStrategy._
import scala.concurrent.duration._

import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._
import akka.pattern.ask
import akka.util.Timeout
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration
import com.typesafe.config.ConfigFactory

import akka.http.scaladsl.marshalling.ToResponseMarshallable

import scala.util.{Success, Failure}

// Classes related to Response
case class BidResponse(id: String, bidRequestId: String, price: Double, adid: Option[String], banner: Option[Banner])

// Classes related to Request
case class BidRequest(id: String, imp: Option[List[Impression]], site: Site, user: Option[User], device: Option[Device])

case class Impression(id: String, wmin: Option[Int], wmax: Option[Int], w: Option[Int], hmin: Option[Int], hmax: Option[Int], h: Option[Int], bidFloor: Option[Double])

case class Site(id: Int, domain: String)

case class User(id: Int, geo: Option[Geo])

case class Device(id: String, geo: Option[Geo])

case class Geo(country: Option[String], city: Option[String], lat: Option[Double], lon: Option[Double])

// Classes related to Campaign
// As data type of targetedSiteIds we can use scala.collection.mutable.TreeSet instead of List here
// TreeSet use a mutable red-black tree as underlying data structure.
// So it's very fast for searching, O(lg(n)) at worst case 
case class Targeting(cities: List[String], targetedSiteIds: List[Int])

case class Banner(id: Int, src: String, width: Int, height: Int)

case class Campaign(id: Int, userId: Int, country: String, targeting: Targeting, banners: List[Int], bid: Double)

object Server {  
  // needed to run the route
  implicit val system = ActorSystem("RTB-System")
  implicit val materializer = ActorMaterializer()
  // needed for the future map/flatmap in the end and future in fetchItem and saveOrder
  implicit val executionContext = system.dispatcher

  var banners: List[Banner] = Nil
  var sites: List[Site] = Nil
  var geos: List[Geo] = Nil
  var devices: List[Device] = Nil
  var campaigns: List[Campaign] = Nil

  implicit val timeout = Timeout(Duration(1, TimeUnit.SECONDS))
  
  // formats for unmarshalling and marshalling
  implicit val siteFormat = jsonFormat2(Site)
  implicit val geoFormat = jsonFormat4(Geo)
  implicit val deviceFormat = jsonFormat2(Device)
  implicit val bannerFormat = jsonFormat4(Banner)
  implicit val targetingFormat = jsonFormat2(Targeting)  
  implicit val campaignFormat = jsonFormat6(Campaign)
  implicit val bidResponseFormat = jsonFormat5(BidResponse)

  implicit val userFormat = jsonFormat2(User)
  implicit val impressionFormat = jsonFormat8(Impression)
  implicit val bidRequestFormat = jsonFormat5(BidRequest)

  class ListUpdateActor extends Actor {
    def receive = {
      case b:Banner => 
        banners = banners :+ b
        sender ! banners.size + "th banner created\n"
      case s:Site => 
        sites = sites :+ s
        sender ! sites.size + "th site created\n"
      case g:Geo => 
        geos = geos :+ g
        sender ! geos.size + "th geo created\n"
      case d:Device => 
        devices = devices :+ d
        sender ! devices.size + "th device created\n"
      case c:Campaign => 
        campaigns = campaigns :+ c
        sender ! campaigns.size + "th campaign created\n"
    }
  }

  class BidMatcherActor extends Actor {
    override val supervisorStrategy =
      OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1 minute) {
        case _: NullPointerException     => Restart
        case _: NoSuchElementException   => Restart
        case _: IllegalArgumentException => Stop
        case _: Exception                => Escalate
      }

    def doesImpressionMatch(i:Impression, b: Banner, bid: Double): Boolean = {
      return (Some(b.height) == i.h && Some(b.width) == i.w ) && i.bidFloor == Some(bid)
    }

    def matchBanner(imp:Option[List[Impression]], bannerIds: List[Int], bid: Double): Boolean = {
      var givenBanners = banners.filter(b => bannerIds.contains(b.id))
      var matchedBanner = givenBanners.find(b => imp.get.find(i => doesImpressionMatch(i, b, bid)) != None)
      return matchedBanner != None
    }

    def receive = {
      case request: BidRequest => {
        var campaign = campaigns.find(c => matchBanner(request.imp, c.banners, c.bid))
        if (campaign != None) {
          sender ! BidResponse("resp1", request.id, campaign.get.bid, Some(campaign.get.id.toString), None)
        } else {
          sender ! BidResponse("NOT Found", "", 0, None, None)
        }
      }
    }
  }

  val listUpdateActor = system.actorOf(Props(new ListUpdateActor()))
  val bidMatcherActor = system.actorOf(Props(new BidMatcherActor()))
  
  def main(args: Array[String]) {

    val route: Route =
      concat(
        post {
          path("match") {
            entity(as[BidRequest]) { bidRequest => complete((bidMatcherActor ? bidRequest).mapTo[BidResponse]) }
          }
        },
        post {
          path("create-banner") {
            entity(as[Banner]) { banner => complete((listUpdateActor ? banner).mapTo[String]) }
          }
        },
        post {
          path("create-geo") {
            entity(as[Geo]) { geo =>
              complete((listUpdateActor ? geo).mapTo[String])
            }
          }
        },
        post {
          path("create-site") {
            entity(as[Site]) { site =>
              complete((listUpdateActor ? site).mapTo[String])
            }
          }
        },
        post {
          path("create-campaign") {
            entity(as[Campaign]) { campaign =>
              complete((listUpdateActor ? campaign).mapTo[String])
            }
          }
        },
        post {
          path("create-device") {
            entity(as[Device]) { device =>
              complete((listUpdateActor ? device).mapTo[String])
            }
          }
        }
      )

    val config = ConfigFactory.load()
    Http().bindAndHandle(route, config.getString("http.interface"), config.getInt("http.port"))
    println(s"Server online at http://localhost:8080/")
  }
}